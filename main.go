package main

import (
	log "github.com/sirupsen/logrus"
	"web_server/server"
	"web_server/settings"
)

func main() {
	log.Info("System booting up")
	settings := settings.ReadSettings("settings/settings.json")

	if settings == nil {
		return
	}

	server.Main(settings)
}

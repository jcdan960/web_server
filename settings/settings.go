package settings

import (
	"io/ioutil"
	log "github.com/sirupsen/logrus"
	"encoding/json"
)
type Settings struct{
	Port uint16 `json:"Port"`
}

func ReadSettings(settingsFile string) *Settings {

	file, err := ioutil.ReadFile(settingsFile)

	if err != nil {
		log.Error("Error loading settings file")
		log.Error(err)
		return nil
	}

	settings := Settings{}
	_ = json.Unmarshal([]byte(file), &settings)

	return &settings
}


package dbManager


import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	_ "github.com/lib/pq"
	"fmt"
)

var DB *sql.DB
type EventData struct {
	Timestamp uint64 `json:"Timestamp"`
	Latitude float32 `json:"Latitude"`
	Longitude float32 `json:"Longitude"`
	IsEntering bool `json:"isEntering"`
}

const (
	host     = "localhost"
	port     = 5432
	user     = "db_manager"
	password = "superpass"
	dbname   = "smart_bus"
	tablename = "event"
)

func CloseDB(){
	defer DB.Close()
}

func ConnectToDB() {

	log.Info("Connecting to db...")
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	DB = db
	log.Info("Successfully connected to Database")
}

func AddDataToDB(dat *EventData){
	var userid int

	query := fmt.Sprintf(`INSERT INTO %s(timestamp, longitude, latitude, isentering) VALUES(%d, %.4f, %.4f, %t);`, tablename, dat.Timestamp, dat.Longitude, dat.Latitude, dat.IsEntering)
	err := DB.QueryRow(query).Scan(&userid)

	if err != nil {
		log.Error(err)
	}
}
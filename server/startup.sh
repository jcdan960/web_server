#!/bin/bash
sudo apt-get -y install postgresql
sudo service postgresql start
sudo passwd postgres
su postgres
psql

CREATE DATABASE smart_bus;
CREATE role db_manager WITH LOGIN PASSWORD 'superpass';
GRANT ALL PRIVILEGES ON DATABASE Production TO db_manager;
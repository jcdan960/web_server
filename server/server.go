package server

// inspired by:
// https://tutorialedge.net/golang/creating-restful-api-with-golang/
//and https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"fmt"
	"io/ioutil"
	"net/http"
	"github.com/gorilla/mux"
	db "web_server/server/dbManager"
	"web_server/settings"
)

type AllEventData []db.EventData

func getAllEvents(w http.ResponseWriter, r *http.Request){
	allEvents := AllEventData{
		db.EventData{27, 12,  25,  true},
	}

	log.Info("Sending all data points")
	json.NewEncoder(w).Encode(allEvents)
}

func homePage(w http.ResponseWriter, r *http.Request){
	log.Info("Endpoint Hit: homePage")
	fmt.Fprintf(w,"Endpoint Hit: homePage")
}

func createNewEvent(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w,"New data posted!")
	reqBody, _ := ioutil.ReadAll(r.Body)
	fmt.Fprintf(w, "%+v", string(reqBody))

	var dat db.EventData
	json.Unmarshal(reqBody, &dat)
	log.Info(dat)
	db.AddDataToDB(&dat)
}

func handleRequests(Port uint16) {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/allevents", getAllEvents)
	myRouter.HandleFunc("/PostData", createNewEvent).Methods("POST")
	log.Info(fmt.Sprintf("Server listening on port %d", Port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", Port), myRouter))
}

func Main(set* settings.Settings) {
	db.ConnectToDB()
	handleRequests(set.Port)
	db.CloseDB()
}
